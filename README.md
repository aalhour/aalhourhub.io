# aalhour.github.io

Forked from jmcglone.github.io

Powered by Jekyll (2.5.3), and Twitter Bootstrap (3.3.4).

If you want to fork this project, make sure you either update the _includes/google_analytics.html page with your own Google Analytics user information or comment the whole code. Otherwise, I will be receiving reporting data from Google about your blog.

